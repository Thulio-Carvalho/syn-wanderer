from stages.entity_stage import EntityStage
from stages.endpoint_stage import EndpointStage
from stages.env_stage import EnvStage
from stages.pages_stage import PagesStage
# from stages.route_stage import RouteStage
from stages.service_stage import ServiceStage
from stages.stores_stage import StoresStage
from stages.route_stage import RouteStage
from utils import *
import sys


class StageCycler:        
    
    def __init__(self, table_name, be_path, fe_path):
        self.data = {}
        self.data['TABLE_NAME'] = table_name
        self.data['BE_PATH'] = be_path
        self.data['FE_PATH'] = fe_path
        self.data['ENTITY_NAME'] = ''
        self.data['ENDPOINT'] = ''
        self.data['ENV_NAME'] = ''
        self.data['SERVICE_PATH'] = ''
        self.data['STORES'] = []
        self.data['PAGES'] = {}
        self.data['ROUTES'] = {}

    def run_cycle(self):
        for stage_name, stage_class in self.STAGE_ORDER:
            stage_instance = stage_class(self.data)

            new_data = stage_instance.run()

            self.data = new_data

            yield self.data

    STAGE_ORDER = [('ENTITY',    EntityStage),
                   ('ENDPOINT',  EndpointStage),
                   ('ENV',       EnvStage),
                   ('SERVICE',   ServiceStage),
                   ('STORES',    StoresStage),
                   ('PAGES',     PagesStage),
                   ('ROUTE',     RouteStage)]

from stage_cycler import StageCycler
from utils.colors import *
from utils.utils import *
from utils.printer import *
from pprint import pprint
from time import sleep
from os import path
import logging
import sys
import csv

BE_PATH = ''
FE_PATH = ''
DELAY = True
DELAY_TIME = 1
MODULE = ''
TABLES_FILE = ''
SAVING_FILE = ''
FILE_EXT = '.csv'
PRE_SAVE_HAPPENED = False
ROUTES_DIR = './routes/'
TABLES_DIR = './module_tables/'
CUSTOM_TABLE = ''
DONE_TABLES = []


def handle_argv():
    global FE_PATH
    global BE_PATH
    global MODULE
    global TABLES_FILE
    global SAVING_FILE
    global CUSTOM_TABLE

    if (len(sys.argv) - 1) < 1:
        print('Usage: $ python main.py <REPO_PATH> OPTIONAL:<CUSTOM_TABLES_FILE>')
        sys.exit()

    REPO_PATH = sys.argv[1]

    if len(sys.argv) > 2:
        CUSTOM_TABLE = sys.argv[2]

    if 'corporativo' in REPO_PATH:
        MODULE = 'CORPORATIVO'
        TABLES_FILE = TABLES_DIR + 'solfis-corporativo.txt'
        SAVING_FILE = ROUTES_DIR + 'solfis-corporativo_routes' + FILE_EXT
    elif 'icms' in REPO_PATH:
        MODULE = 'ICMS-IPI'
        TABLES_FILE = TABLES_DIR + 'solfis-icms-ipi.txt'
        SAVING_FILE = ROUTES_DIR + 'solfis-icms-ipi_routes' + FILE_EXT
    elif 'iss' in REPO_PATH:
        MODULE = 'ISS'
        TABLES_FILE = TABLES_DIR + 'solfis-iss.txt'
        SAVING_FILE = ROUTES_DIR + 'solfis-iss_routes' + FILE_EXT
    else:
        print('Módulo inválido')
        sys.exit()

    if CUSTOM_TABLE != '':
        TABLES_FILE = CUSTOM_TABLE

    def has_bar(s): return s[len(s) - 1] == '/'

    BE_PATH = REPO_PATH + ('' if has_bar(REPO_PATH) else '/') + 'api/src/'
    FE_PATH = REPO_PATH + ('' if has_bar(REPO_PATH) else '/') + 'public/src/'


def ask_question(msg):
    choice = input(BOLD + '\n' + msg + ' [y|n]' + RESET)

    while (choice.lower() not in ['y', 'n']):
        print('Invalid option')
        choice = input(BOLD + '\n' + msg + ' [y|n]' + RESET)

    return choice.lower() == 'y'


def pre_save():
    global SAVING_FILE
    global DONE_TABLES

    choosen_file = SAVING_FILE

    if path.exists(choosen_file):
        question = 'Found an previous execution, continue progress? (File:' + \
            choosen_file + ')'
        if not ask_question(question):
            cnt = 1
            while path.exists(choosen_file):
                choosen_file = SAVING_FILE[:len(
                    SAVING_FILE) - 4] + '_' + str(cnt) + FILE_EXT
                cnt += 1

            SAVING_FILE = choosen_file

    if path.exists(SAVING_FILE):
        with open(SAVING_FILE) as f:
            lines = f.readlines()
        if len(lines) > 1:
            for i in range(1, len(lines)):
                print('.')
                DONE_TABLES.append(lines[i].split(',')[0])
    else:
        with open(SAVING_FILE, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=['TABLE_NAME', 'ROUTES'])
            writer.writeheader()


def save_routing(routes, table):
    data = {'TABLE_NAME': table, 'ROUTES': []}

    for route_list in routes.values():
        for route in route_list:
            data['ROUTES'].append(route)

    with open(SAVING_FILE, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=['TABLE_NAME', 'ROUTES'])
        writer.writerow(data)


def main():
    printer = Printer()
    handle_argv()
    pre_save()

    with open(TABLES_FILE) as f:
        tables = f.readlines()

    tables = list(map(lambda s : s[:len(s)-1], tables))

    for done in DONE_TABLES:
        tables.remove(done)

    for table in tables:

        if table != '':

            print(BOLD + 'NEW RUN' + RESET)

            stage_cycler = StageCycler(table, BE_PATH, FE_PATH)

            printer.print_var('MODULE', MODULE)
            printer.print_var('TABLE_NAME', table)
            printer.print_var('BE_PATH', BE_PATH,)
            printer.print_var('FE_PATH', FE_PATH,)

            cycler = stage_cycler.run_cycle()

            stage_names = [name for name, _ in stage_cycler.STAGE_ORDER]
            stage_names.append('FINISH')

            s_idx = -1

            while True:
                try:
                    s_idx += 1

                    print('\n')
                    print(BOLD + 'RUNNING STAGE: ' +
                          CYAN + stage_names[s_idx] + RESET)

                    data = next(cycler)

                    print(BOLD + 'FINISHED RUNNING STAGE: ' +
                          CYAN + stage_names[s_idx] + RESET)
                    print('\n')

                except StopIteration:
                    print(BOLD + 'FINISHED CYCLING THROUGH STAGES' + RESET)
                    break

                except Exception as e:
                    print(e)
                    break

        save_routing(data['ROUTES'], table)

        if not ask_question('Continue to next table?'):
            sys.exit()


if __name__ == "__main__":
    main()

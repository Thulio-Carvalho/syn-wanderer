from utils.colors import *
from utils.utils import *
from utils.printer import Printer
from stage_cycler import StageCycler
from time import sleep
import logging
import sys


class Finder:
    def __init__(self, repo, table):
        self.REPOSITORY = repo
        self.TABLE_NAME = table
        self.__build_finder()

    def __build_finder(self):
        self.log = logging.getLogger("proxy-maker-logger")
        self.log.info("Building Finder")

        self.printer = Printer()

        def has_bar(s): return s[len(s) - 1] == '/'
        self.BE_PATH = self.REPOSITORY + \
            ('' if has_bar(self.REPOSITORY) else '/') + 'api/src/'
        self.FE_PATH = self.REPOSITORY + \
            ('' if has_bar(self.REPOSITORY) else '/') + 'public/src/'

        self.STAGE_CYCLER = StageCycler(
            self.TABLE_NAME, self.BE_PATH, self.FE_PATH)

        self.printer.print_var('TABLE_NAME', self.TABLE_NAME)
        self.printer.print_var('BE_PATH', self.BE_PATH,)
        self.printer.print_var('FE_PATH', self.FE_PATH,)

    def run(self):
        cycle_gen = self.STAGE_CYCLER.run_cycle()

        stage_names = [name for name, _ in self.STAGE_CYCLER.STAGE_ORDER]
        stage_names.append('FINISH')

        s_idx = -1

        while True:
            try:
                s_idx += 1

                print('\n')
                print(BOLD + 'RUNNING STAGE: ' +
                      CYAN + stage_names[s_idx] + RESET)

                next(cycle_gen)

                print(BOLD + 'FINISHED RUNNING STAGE: ' +
                      CYAN + stage_names[s_idx] + RESET)
                print('\n')

            except StopIteration:
                print(BOLD + 'FINISHED CYCLING THROUGH STAGES' + RESET)
                break

            except Exception as e:
                logging.exception(BOLD + 'ERROR AT STAGE: ' +
                                  CYAN + stage_names[s_idx] + RESET)
                break

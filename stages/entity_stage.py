from utils import utils
from utils.printer import *


# Deve encontrar o nome da entidade no backend

class EntityStage:

    def __init__(self, data):
        self.data = data

        m_preffix = 'metaDataWrapper.setTableName("'
        m_suffix = '"'
        self.match = m_preffix + self.data['TABLE_NAME'] + m_suffix
        self.printer = Printer()

    def run(self):

        entity_file = utils.find_on_dir(self.data['BE_PATH'], self.match)[0]

        self.data['ENTITY_NAME'] = self._extractEntityName(entity_file)

        self.printer.print_var('ENTIDADE', self.data['ENTITY_NAME'], is_dir=False)

        return self.data

    def _extractEntityName(self, filename):
        for i in range(len(filename)-1, -1, -1):
            if filename[i] == '/':
                return filename[i+1: filename.find(".java") - len("Repository")]

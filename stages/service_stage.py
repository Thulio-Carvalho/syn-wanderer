from utils import utils
from utils.printer import *


class ServiceStage:

    def __init__(self, data):
        self.data = data

        self.match = 'envs.' + self.data['ENV_NAME']
        self.printer = Printer()

    def run(self):

        self.data['SERVICE_PATH'] = utils.find_on_dir(
            self.data['FE_PATH'], self.match, choosable=True, only_one=True)[0]

        self.printer.print_var('SERVICE_PATH', self.data['SERVICE_PATH'], is_dir=True)

        return self.data

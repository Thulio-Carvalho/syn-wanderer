from utils import utils
from utils.printer import *


# Deve encontrar o Env relacionado ao endpoint no frontend


class EnvStage:

    def __init__(self, data):
        self.data = data

        self.env_file = self.data['FE_PATH'] + 'constants/ApiEndPoints.js'
        self.custom_endpoint = "'" + self.data['ENDPOINT'] + "'"
        self.printer = Printer()


    def run(self):

        with open(self.env_file) as env_file:
            for line in env_file.readlines():
                endpoint_idx = line.find(self.custom_endpoint)
                if endpoint_idx != -1:
                    self.data['ENV_NAME'] = line[2:endpoint_idx - 2]

        self.printer.print_var('ENV_NAME', self.data['ENV_NAME'])

        return self.data

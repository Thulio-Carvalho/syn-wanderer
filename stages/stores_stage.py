from utils import utils
from utils.printer import *
# Deve encontrar as Stores que importam o SERVICE_PATH


class StoresStage:

    def __init__(self, data):
        self.data = data

        self.stores_path = self.data['FE_PATH'] + 'stores/'

        match = self.data['SERVICE_PATH'][len(self.data['FE_PATH']):]
        self.match = match[:len(match) - 3]
        self.printer = Printer()

    def run(self):

        self.data['STORES'] = utils.find_on_dir(self.stores_path, self.match)

        for store in self.data['STORES']:
            self.printer.print_var('STORE', store, is_dir=True)

        return self.data

from utils import utils
from utils.printer import *

# Deve encontrar o endpoint relacionado à entidade no backend

class EndpointStage:

    def __init__(self, data):
        self.data = data

        c_preffix = '/'
        c_suffix = 'Controller'
        self.controller_name = c_preffix + self.data['ENTITY_NAME'] + c_suffix
        self.req_mapping = "@RequestMapping("
        self.printer = Printer()

    def run(self):

        f = utils.search_file_by_name(
            self.data['BE_PATH'], self.controller_name)

        if f == None:
            print('Controller não encontrado')
            raise Exception()

        with open(f.name) as f:
            lines = f.readlines()
            for line in lines:
                req_idx = line.find(self.req_mapping)
                if req_idx != -1:
                    self.data['ENDPOINT'] = line[(
                        req_idx + len(self.req_mapping) + 2): len(line) - 3]

        self.printer.print_var('ENDPOINT', self.data['ENDPOINT'])

        return self.data

from utils import utils
from utils import colors

# Deve encontrar a rota das component pages


class RouteStage:

    def __init__(self, data):
        self.data = data

        self.pages_path = self.data['FE_PATH'] + 'pages/'

    def run(self):

        for page in self.data['COMPONENT_PAGES'].keys():
            for component_page in self.data['COMPONENT_PAGES'][page]:

                utils.print_var('COMPONENT_PAGE', component_page)

                # match = 'component={' + component_page + '}'

                match = component_page[len(self.data['FE_PATH']):]
                match = match[:len(match) - 3]

                files = utils.find_matching_files(self.pages_path, match)

                if len(files) == 0:
                    print("Não foram encontrados arquivos com match " + match)

                cnt = 1
                for f in files:
                    print_var('ROUTE_FILE', f.name, multiple=True, is_dir=True)

                    cnt += 1
                    # route = _getRouteByComponent(f.name, component_page)
                    if route != None:
                        print(route)
        return self.data

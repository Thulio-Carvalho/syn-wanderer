from utils import utils
from utils import colors
from utils.printer import *

# Deve encontrar a rotas das pages


class RouteStage:

    def __init__(self, data):
        self.data = data

        self.pages_path = self.data['FE_PATH'] + 'pages/'
        self.printer = Printer()

    def run(self):

        for store in self.data['PAGES'].keys():
            pages = self.data['PAGES'][store]

            for page in pages:

                if page not in self.data['ROUTES']:
                    self.data['ROUTES'][page] = []

                self.printer.print_var('PAGE', page, is_dir=True)

                if self._isFormPage(page):
                    page_name = self._getNameByClass(page)
                    print(page_name)

                    page_match = 'import ' + page_name + ' '

                    formFiles = utils.find_matching_files(
                        self.pages_path, page_match)

                    for f in formFiles:

                        self.printer.print_var('SUB_PAGE (FORM)', f.name, multiple=True, is_dir=True)

                        route_files = utils.find_matching_files(
                            self.pages_path, self._treatFileName(f.name))

                        for rf in route_files:
                            route_name = self._getNameByImport(rf.name, f.name)
                            route = self._getRoute(rf.name, route_name)
                            self.data['ROUTES'][page].append(route)
                            self.printer.print_var('ROUTE', route, multiple=True)
                else:
                    match = self._treatFileName(page)
                    
                    files = utils.find_matching_files(self.data['FE_PATH'], match)

                    for f in files:
                        route_name = self._getNameByImport(f.name, match)
                        route = self._getRoute(f.name, route_name)
                        self.data['ROUTES'][page].append(route)

                        self.printer.print_var('ROUTE', route, multiple=True)

        return self.data

    def _treatFileName(self, name):

        if name[:2] != '..':
            return name

        ret = name[len(self.data['FE_PATH']):]
        return ret[:len(ret) - 3]

    def _isFormPage(self, filename):
        return '/form.js' in filename

    def _getNameByImport(self, f, path):
        match = self._treatFileName(path)
        return self._getName(f, match, '', '', 'import ', ' from')

    def _getNameByClass(self, f):
        return self._getName(f, 'extends', '', '', 'class ', ' extends')

    def _getRoute(self, f, route_name):
        return self._getName(f, route_name, '{', '}', 'path={`', '`}', reverse=False)

    # Encontra nome
    def _getName(self, f, pattern, prefix, suffix, match1, match2, reverse=True):
        match = prefix + pattern + suffix

        with open(f) as f:
            text = f.read()

        match_idx = text.find(match)
        if reverse:
            for i in range(match_idx - 1, -1, -1):
                subtext = text[i:match_idx]
                m1_idx = subtext.find(match1)
                if m1_idx != -1:
                    m2_idx = subtext.find(match2)
                    return subtext[m1_idx + len(match1): m2_idx]
        else:
            subtext = text[match_idx:]
            m1_idx = subtext.find(match1)
            m2_idx = subtext.find(match2)
            return subtext[m1_idx + len(match1): m2_idx]
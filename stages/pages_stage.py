from utils import utils
from utils import colors
from utils.printer import *

# Deve encontrar as Pages que importam as Stores


class PagesStage:

    def __init__(self, data):
        self.data = data

        self.pages_path = self.data['FE_PATH'] + 'pages/'
        self.printer = Printer()

    def run(self):

        for store in self.data['STORES']:
            self.printer.print_var('STORE', store, is_dir=True)

            if store not in self.data['PAGES']:
                self.data['PAGES'][store] = []

            match = store[len(self.data['FE_PATH']):]
            match = match[:len(match) - 3]

            files = utils.find_matching_files(self.pages_path, match)

            if len(files) == 0:
                print("Não foram encontrados arquivos com match\n " + match + '\n')
                continue


            cnt = 1
            for f in files:
                self.data['PAGES'][store].append(f.name)

                self.printer.print_var('PAGE', f.name, multiple=True, is_dir=True)
                cnt += 1

        return self.data

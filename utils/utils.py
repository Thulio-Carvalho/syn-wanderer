import sys
from pprint import pprint
from utils.colors import *
from utils.printer import *
import glob
import os


def find_on_file(string, f, limit_occur=True):
    occurrences = []
    for line in f.readlines():

        i = line.find(string)
        if i != -1:
            outline = line[0:i] + line[i:i +
                                       (len(string))] + line[i + len(string):len(line)]
            occurrences.append(outline)
            if limit_occur:
                break
    return occurrences


def file_traveler(d):
    files = list()
    for (dirpath, dirnames, filenames) in os.walk(d):
        files += [os.path.join(dirpath, file) for file in filenames]
    for filepath in files:
        with open(filepath, 'r', encoding="utf8", errors='ignore') as f:
            yield f


def dir_finder(d, pattern):
    total_occurs = []
    traveler = file_traveler(d)

    for f in traveler:

        occurrences = find_on_file(pattern, f)

        if len(occurrences) > 0:
            total_occurs.append((f, occurrences))

    return total_occurs


def find_matching_files(path, pattern):
    return [f for (f, _) in dir_finder(path, pattern)]

# Procura um arquivo com um padrão no nome


def search_file_by_name(path, name):
    traveler = file_traveler(path)

    for f in traveler:
        if name in f.name:
            return f
    return None


def find_on_dir(path, match, choosable=True, only_one=False):
    matchs = find_matching_files(path, match)

    print(path)
    print(match)

    if len(matchs) == 0:
        raise Exception("Não encontrado nenhum match")

    print("Encontrados matchs, (" + str(len(matchs)) + "):")

    cnt = 1

    for f in matchs:
        print(BOLD + "[" + str(cnt) + "]" + RESET +
              Printer.dir_str_postprocess(f.name))
        cnt += 1

    if len(matchs) > 1:
        if choosable:

            has_valid_answer = False

            while not has_valid_answer:
                if only_one:
                    cnt = 1

                    print("Escolha o número: ")
                    choice = input()

                    if int(choice) not in range(1, len(matchs) + 1):
                        print("Resposta inválida")
                        continue
                    else:
                        has_valid_answer = True

                    return [matchs[int(choice) - 1].name]

                else:
                    print("Escolha o várias separadas por vírgulas ex: 1,2,3 ")
                    print("Ou continue para selecionar todas")

                    choices = input().split(',')

                    if choices == ['']:
                        choices = [str(i) for i in range(1, len(matchs) + 1)]

                    choices = list(map(int, choices))

                    for i in choices:
                        if int(i) not in range(1, len(matchs) + 1):
                            print("Resposta inválida")
                            continue

                    has_valid_answer = True

                    return [matchs[i-1].name for i in choices]
        else:
            return [m.name for m in matches]
    else:
        return [matchs[0].name]

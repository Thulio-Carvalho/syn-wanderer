import logging
from utils.colors import *


class Printer:
    def __init__(self):
        self.logger = logging.getLogger('proxy-maker-logger')

    def print_var(self, name, value, is_dir=False, multiple=False):
        if value == None or value == '':
            raise Exception('Not valid value for ' + name)
        string = ''
        if multiple:
            string += BOLD + GREEN
        else:
            string += BOLD + RED
        string += name + ': '
        if is_dir:
            string += Printer.dir_str_postprocess(value)
        else:
            string += RESET + BLUE + value + RESET

        # TODO use logger
        print(string)
        # self.logger.info(string)

    @staticmethod
    def dir_str_postprocess(s):
        if len(s) < 3:
            return s
        slashes_pos = []
        second_slash_idx = s[3:].find('/')
        s = s[second_slash_idx + 3:]
        for i in range(len(s)):
            if s[i] == '/':
                slashes_pos.append(i)
        if len(slashes_pos) > 4:
            pos1 = slashes_pos[3] + 1
            pos2 = slashes_pos[len(slashes_pos) - 2]
            s = BLUE + s[:pos1] + GREEN + '...' + BLUE + s[pos2:] + RESET
        else:
            s = BLUE + s + RESET
        return s

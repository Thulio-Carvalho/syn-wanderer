from utils.colors import *
from utils.utils import *
from stage_cycler import StageCycler
from time import sleep
from finder import Finder
import logging
import sys

TABLE_NAME = ''
REPO_PATH = ''


def handle_argv():
    global FE_PATH
    global BE_PATH
    global TABLE_NAME
    global REPO_PATH

    if (len(sys.argv) - 1) != 2:
        print('Usage: $ python main.py <REPO_PATH> <TABLE_NAME>')
        sys.exit()

    REPO_PATH = sys.argv[1]
    TABLE_NAME = sys.argv[2]


def main():

    handle_argv()
    finder = Finder(REPO_PATH, TABLE_NAME)
    finder.run()


if __name__ == "__main__":
    main()
